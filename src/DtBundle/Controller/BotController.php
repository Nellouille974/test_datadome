<?php

namespace DtBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;


class BotController extends Controller
{
    public function listAction()
    {
        $bots = $this->getDoctrine()->getRepository('DtBundle:Bot')->findAll();
        $serviceJson = $this->get('dt.json');
        
        $data = $serviceJson->format($bots);

        $response = new JsonResponse();
        return $response->setData($data);
    }

    public function detailAction(Bot $bots)
    {

    }
}
