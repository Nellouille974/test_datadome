<?php

namespace DtBundle\Services;

class Json
{
    public function format($data)
    {

        $formatedData = [];

        foreach ($data as $value) {
            $formatedData[] = [
                "id" => $value->getId(),
                "name" => $value->getName(),
                "type" => $value->getType(),
                "created_at" => $value->getCreatedAt()->format('d-m-Y')
            ];
        }

        return $formatedData;
    }
}
